using UnityEngine;
using System.Collections;

public class DemoDoorScript : MonoBehaviour{
	[SerializeField] private Animator myDoor = null;


	void OnEnable(){
		PinPad.open+= rotate;
	}

	void OnDisable(){
		PinPad.open-= rotate;
	}



	public void rotate(){
		myDoor.Play("DoorClose",0,0.0f);
	}
}