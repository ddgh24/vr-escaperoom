using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingAudio : MonoBehaviour
{

    CharacterController footsteps;
    public AudioSource walking;

    // Start is called before the first frame update
    void Start()
    {
        footsteps = GetComponent<CharacterController>();
        walking.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (footsteps.isGrounded == true && footsteps.velocity.magnitude > 2f && walking.isPlaying == false)
        {
            walking.volume = Random.Range(0.6f, 0.9f);
            walking.pitch = Random.Range(0.8f, 1.1f);
            walking.Play();
        }
    }

}
