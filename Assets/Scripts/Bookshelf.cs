﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bookshelf : ClickableObject
{
    [SerializeField] private Animator myDoor = null;
    public GameObject book;
    public AudioSource funkybookshelf;
    public AudioClip placebook;
    public AudioClip open;

    protected override void OnClickWithObject(string itemName)
    {
        switch (itemName)
        {
            case "book":
                Debug.Log("the book has been interacted");
                funkybookshelf.PlayOneShot(placebook);
                book.SetActive(false);
                Destroy(PlayerPickUp.Instance.HeldItem);

                funkybookshelf.PlayOneShot(open);
                myDoor.Play("DoorOpen", 0, 0.0f);
                break;

            case null:
                break;

            default:
                Debug.LogError("There is an object in objectsWithInteractions that has no interaction defined in OnClickWithItem");
                break;
        }
    }

}