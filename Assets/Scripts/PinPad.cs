using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PinPad : MonoBehaviour
{

	public delegate void EntryAllowed();
	public static event EntryAllowed open;

	public string passcode = "1234";
	private string userInput = "";

	public int inputLength = 4;

	public GameObject radio;

	public AudioClip beepSound;
	public AudioClip failSound;
	public AudioClip successSound;

	public Text display;
	public Image image;

	public bool isradio;
	public bool isdoor;

	public GameObject fade;

	AudioSource audioSource;

	bool done = false;

	public string prevInput;

	private void Start()
	{
		userInput = "";
		audioSource = GetComponent<AudioSource>();
	}

	public void ButtonClicked(string n)
	{
		prevInput = n;
		Debug.Log(n);
		audioSource.PlayOneShot(beepSound);
		userInput += n;
		Debug.Log(userInput);
		display.text = userInput;
		//AudioSource radioSource = radio.GetComponent<AudioSource>();

		if (done == false)
		{
			if (userInput.Length >= 4)
			{

				if (userInput == passcode)
				{
					audioSource.PlayOneShot(successSound);
					Debug.Log("Correct passcode");
					image.GetComponent<Image>().color = new Color32(0, 255, 0, 100);

					if (isdoor)
					{
						GameObject.Find("EscRoom-Player Variant").GetComponent<OVRPlayerController>().Acceleration = 0;
						fade.SetActive(true);
						StartCoroutine(wait());
						
					}
					else if (isradio)
					{
						radio.GetComponent<RadioInteraction>().correctPin = true;


					}



					done = true;
					//radioSource.PlayOneShot(correctClue);


				}
				else
				{
					audioSource.PlayOneShot(failSound);
					Debug.Log("Incorrect passcode. Try again");
					image.GetComponent<Image>().color = new Color32(255, 0, 0, 100);
					userInput = "";
					//radioSource.PlayOneShot(incorrectStatic);

				}
			}
		}
		else
		{
			Debug.Log("You already have the correct password, what are you doing??");
			image.GetComponent<Image>().color = new Color32(255, 255, 255, 100);
			display.text = "";
			userInput = "";
		}
	}

	IEnumerator wait()
    {
		open();
		yield return new WaitForSeconds(5f);
		
		SceneManager.LoadScene("Credits");
	}
}