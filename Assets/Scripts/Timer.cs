using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    public float timeStart = 60;
    public Text textBox;
    public AudioSource funkytimer;
    public AudioClip gameover;

    private bool played = false;

    void Start()
    {
        textBox.text = timeStart.ToString();
    }

    void Update()
    {
        timeStart -= Time.deltaTime;
        float minutes = Mathf.FloorToInt(timeStart / 60);
        float seconds = Mathf.FloorToInt(timeStart % 60);
        textBox.text = string.Format("{0:00}:{1:00}", minutes, seconds);

        if (textBox.text == "00:00")
        {
            if (!played)
            {
                funkytimer.PlayOneShot(gameover);
                played = true;
            }
            SceneManager.LoadScene("GameOver");
        }
    }
}
