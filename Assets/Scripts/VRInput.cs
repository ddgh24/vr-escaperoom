using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VRInput
{
    //input from joystick
    public static Vector2 GetMovement()
    {
        Vector2 vrInput = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick);
        if (vrInput != Vector2.zero) return vrInput;
        return OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick);
        // if (VRinput != Vector2.zero) return VRinput;
        // return new Vector2(Input.GetAxis("horizontal"), Input.GetAxis("vertical"));
    }
    public static bool Drop()
    {
        return OVRInput.Get(OVRInput.Button.One) ||
            OVRInput.Get(OVRInput.Button.Three) ||
            Input.GetAxis("DropObject") > 0;
    }

    public static bool InteractLeft() 
    { 
        return OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger) > 0; 
    }
    public static bool InteractRight() 
    { 
        return OVRInput.Get(OVRInput.Axis1D.SecondaryIndexTrigger) > 0; 
    }
    public static bool GetRaycastLeft() 
    { 
        return OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger) > 0; 
    }
    public static bool GetRaycastRight() 
    { 
        return OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger) > 0; 
    }
}
