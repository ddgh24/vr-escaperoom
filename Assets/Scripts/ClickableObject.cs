using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickableObject : MonoBehaviour
{
    [SerializeField] protected ObjectInteraction[] objectsWithInteractions;
    [SerializeField] protected bool showDebugMessages = false;
    [SerializeField] Color outlineColor = Color.white;
    [SerializeField] float outlineWidth = 10f;

    protected     bool        isShaking = false;
    protected     Vector3     startPos;
    protected     Animator    animator;
    protected     Outline     outline;
    protected     Hands       pickupHand;
    protected     int         handsHovering = 0;

    public virtual void Start()
    {
        outline = gameObject.AddComponent<Outline>();
        outline.OutlineMode = Outline.Mode.OutlineVisible;
        outline.OutlineColor = outlineColor;
        outline.OutlineWidth = 0; 
        startPos = transform.position;
        animator = gameObject.GetComponent<Animator>();
    }
    public void Interact(PickUpItem heldItem, Hands handtype)
    {
        ObjectInteraction interaction = GetInteraction(heldItem);
        pickupHand = handtype;
        if (interaction != null)
        {
            switch(interaction.reaction)
            {
                case Reaction.AnimationTrigger:
                    if (!animator) {
                        Debug.LogError("ClickableObject " + name + " reacts to " + interaction.itemName + " with an animation, but has no Animator");
                    }
                    animator.SetTrigger(interaction.animationTrigger);
                    break;
                case Reaction.Shake:
                    ShakeObject();
                    break;
                case Reaction.Other:
                    OnClickWithObject(interaction.itemName);
                    break;
            }
        }
        else
        {
            OnClick();
        }
    }
    public void OnHover() { 
        handsHovering++;
        outline.OutlineWidth = outlineWidth;
    }
    public void OnEndHover() { 
        handsHovering --;
        if (handsHovering == 0) { outline.OutlineWidth = 0; }
    }

    protected virtual void OnClick()
    {
        ShakeObject();
    }
    protected virtual void OnClickWithObject(string itemName)
    {
        switch (itemName)
        {
            case "ExampleName":
                ShowMessage("interacted with example item");
                break;
            default:
                Debug.LogError("There is an object in objectsWithInteractions that has no interaction defined in OnClickWithObject");
                break;
        }
    }
    protected void ShakeObject() 
    {
        if (isShaking) return;
        isShaking = true;
        StartCoroutine(Shake());
    
        IEnumerator Shake()
        {
            float shakeTime = 1;
            float numShakes = 5;
            float time = 0;
            float shakeDist = 0.05f;
            while (time < shakeTime)
            {
                float currentDist = shakeDist * Mathf.Sin(time * numShakes * (2 * Mathf.PI));
                transform.position = startPos + new Vector3(currentDist, 0, currentDist);
                yield return new WaitForFixedUpdate();
                time += Time.deltaTime;
            }
            transform.position = startPos;
            isShaking = false;
        }
    }
    
    protected void ShowMessage(string str)
    {
        if (showDebugMessages)
        {
            Debug.Log(str);
        }
    }
    protected ObjectInteraction GetInteraction(PickUpItem heldItem)
    {
        if (!heldItem) { return null; }
        foreach (ObjectInteraction o in objectsWithInteractions)
        {
            if (o.pickUpItem == heldItem) { return o; }
        }
        return null;
    }
}