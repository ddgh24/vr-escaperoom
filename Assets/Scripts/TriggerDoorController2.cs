using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TriggerDoorController2 : MonoBehaviour
{
    [SerializeField] private Animator myDoor = null;
    [SerializeField] private bool openTrigger = false;
    [SerializeField] private bool closeTrigger = false;
    [SerializeField] private float accessTimeOut = 2.0f;

    private bool State = false;
    private float accessTimer = 0.0f;

    [SerializeField] private bool pinpad = false;
    [SerializeField] private bool locke = false;
    [SerializeField] private bool safe = false;

    public AudioSource funkydoor;
    public bool frontdoor;
    public GameObject fade;



    void OnEnable()
    {

        if (pinpad)
        {
            PinPad.open += allowAccess;

        }

        if (locke)
        {
            Lock.open += allowAccess;

        }
        if (safe)
        {
            PinPadSafe.open += allowAccess;

        }
    }

    private void allowAccess()
    {
        accessTimer = accessTimeOut;
        State = true;
        Enter();
    }

    private void FixedUpdate()
    {
        if (State)
        {
            if (accessTimer < 1.0f)
            {
                State = false;
            }
            accessTimer -= Time.fixedDeltaTime;
            
        }

    }

    void OnDisable()
    {

        if (pinpad)
        {
            PinPad.open -= allowAccess;
        }

        if (locke)
        {
            Lock.open -= allowAccess;
        }
        if (safe)
        {
            PinPadSafe.open -= allowAccess;

        }
    }

    private void Enter()
    {
        if (State)
        {
                if (openTrigger)
                {
                    funkydoor.Play();
                    myDoor.Play("DoorOpen", 0, 0.0f);

                    if (frontdoor)
                    {
                        fade.SetActive(true);
                        SceneManager.LoadScene("Credits");
                    }
                }

                else if (closeTrigger)
                {
                    funkydoor.Play();
                    myDoor.Play("DoorClose", 0, 0.0f);
                    if (frontdoor)
                    {
                        fade.SetActive(true);
                        SceneManager.LoadScene("Credits");
                    }
            }
            
        }


    }


}