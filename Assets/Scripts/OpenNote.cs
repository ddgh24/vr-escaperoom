using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class OpenNote : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject uiObj;

    void Start()
    {
        uiObj.SetActive(false);
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            uiObj.SetActive(true);
        }
    }
    void OnTriggerExit(Collider other)
    {
        uiObj.SetActive(false);
    }
}
