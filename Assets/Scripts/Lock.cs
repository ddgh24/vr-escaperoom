using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lock : ClickableObject
{

    [SerializeField] private GameObject padre;
    private GameObject acidObj;
    private Material acid;

    public AudioSource funkylock;

    public delegate void EntryAllowed();
    public static event EntryAllowed open;
    // private Collider parentMesh;
    // private bool collision;


    public override void Start() {
        base.Start();
        acidObj = GameObject.Find ("myPotionPrefab");
        acid = acidObj.GetComponent<Renderer>().material;
        // parentMesh = padre.GetComponent<MeshCollider>();
        // collision = false;
    }


    // void onCollisionEnter(Collision col) 
    // {

    //     if(col.gameObject.tag == "myPotionPrefab")
    //     {
    //         collision = true;
    //         print("collision");
    //     }
    // }

    protected override void OnClickWithObject(string itemName) {

        if (itemName == "myPotionPrefab") {   // && collision check?

            funkylock.Play();
            acidInteraction();
            Debug.Log("Clicked with potion");
        }
    }

    private void acidInteraction() {  
        acid.SetFloat("Vector1_afbed82321df45abb1eccdfcb1bda448", 0.0f);
        Destroy(padre);
        open();
    }

}
