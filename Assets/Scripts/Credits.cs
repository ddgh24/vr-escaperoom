﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Credits : MonoBehaviour
{

    public Animator animator;
    void Update()
    {
        if(animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1){
        SceneManager.LoadScene("Play Menu");
    }
    }
}