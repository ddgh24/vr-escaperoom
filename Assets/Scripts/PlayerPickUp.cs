using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPickUp : MonoBehaviour
{
    //serialized vars
    [Header("Where to hold, where to drop")]
    [Tooltip("How far in front of you should you drop the item?")]
    [SerializeField] float dropDistance = 1;
    [Tooltip("How far below eye level should the object be when you drop it?")]
    [SerializeField] float dropHeightBelowEyelevel = 3;

    [Header("holding item")]
    [SerializeField] Transform leftHand;
    [SerializeField] Transform rightHand;
    [Tooltip("how far from yourself are you holding the object? (x: right, y: up, z: forward)")]
    [SerializeField] Vector3 holdDistance = new Vector3(1, 0, 2);

    [SerializeField] Vector3 inputVector = new Vector3(.1f, .1f, .1f);//sabris

    //private vars
    private PickUpItem      heldItem    = null;

    Vector3 originalScale;//sabris

    private Camera          main;
    private CharacterController character;
    //getters and setters
    public PickUpItem HeldItem { get { return heldItem; } }

    public bool pickedup;

    private void Start()
    {
        main = Camera.main;
        character = GetComponentInChildren<CharacterController>();
    }
    private void Update()
    {

        if (VRInput.Drop())
        {
            DropItem();
        } 
    }

    public void PickUp(PickUpItem item, Hands handtype)
    {
        if (heldItem != null)
        {
            DropItem();
        }



        pickedup = true;
        heldItem = item;
        //turn off physics
        heldItem.GetComponent<Rigidbody>().isKinematic = true;
        heldItem.GetComponentInChildren<Collider>().enabled = false;
        //set parent
        Transform padre = (handtype == Hands.right) ? rightHand : leftHand;
        heldItem.transform.SetParent(padre);
        //set position
        Vector3 heldItemPosition = LocalHoldOffset(padre.transform, holdDistance);
        heldItemPosition += LocalHoldOffset(padre.transform, heldItem.HoldOffset);
        heldItem.transform.localPosition =  heldItemPosition;

        originalScale = heldItem.transform.localScale;//sabris

        heldItem.transform.localScale = inputVector;//sabris
    }

    private void DropItem()
    {
        heldItem.transform.localScale = originalScale;//sabris


        if (!heldItem) { return; }
        //enable physics
        heldItem.GetComponent<Rigidbody>().isKinematic = false;
        heldItem.GetComponentInChildren<Collider>().enabled = true;
        //drop it in a position
        Vector3 itemPosition = main.transform.position + dropDistance * main.transform.forward;
        itemPosition.y = main.transform.position.y - dropHeightBelowEyelevel;
        itemPosition += LocalHoldOffset(heldItem.transform, heldItem.HoldOffset);
        heldItem.transform.position = itemPosition;
        //set back to its original layer
        heldItem.transform.SetParent(null);
        heldItem = null;
        pickedup = false;

        
    }
    Vector3 LocalHoldOffset(Transform t, Vector3 offset)
    {
        Vector3 x = offset.x * t.right;    //x direction is to the right
        Vector3 y = offset.y * t.up;       //y direction is up
        Vector3 z = offset.z * t.forward;  //z direction is forward
        return x + y + z;

    }
    //singleton code
    public static PlayerPickUp Instance;
    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

}