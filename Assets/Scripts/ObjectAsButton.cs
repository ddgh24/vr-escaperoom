using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ObjectAsButton : ClickableObject
{
    public UnityEvent unityEvent = new UnityEvent();
    //public GameObject button;

    // Start is called before the first frame update
    // public override void Start()
    //{
    //    button = this.gameObject;
    // }

    // Update is called once per frame
    // void Update()
    // {
    //     Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    //     RaycastHit hit;
    //     if (Input.GetMouseButtonDown(0))
    //     {
    //         if(Physics.Raycast(ray, out hit) && hit.collider.gameObject == gameObject)
    //         {
    //             unityEvent.Invoke();
    //         }
    //     }
    // }

    protected override void OnClick()
    {
        unityEvent.Invoke();
    }
}
