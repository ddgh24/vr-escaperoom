using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ButtonScript : ClickableObject{

	public int keypadNumber= 1;

	public UnityEvent KeypadClicked;

	public GameObject Keypad;

	public bool safe;


	 protected override void OnClick(){

        string keynum = keypadNumber.ToString();

        if (safe)
        {
			if(Keypad.GetComponent<PinPadSafe>().prevInput!= keynum)
            {
                KeypadClicked.Invoke();
            }
        }
        else
        {
            if (Keypad.GetComponent<PinPad>().prevInput != keynum)
            {
                KeypadClicked.Invoke();
            }
        }
	 	

	 }
}