﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionAudio : MonoBehaviour
{

    public AudioSource collisionAudio;

    // Start is called before the first frame update
  //  void Start()
  //  {
   //     collisionAudio = GetComponent<AudioSource> ();
        
   // }

    // Update is called once per frame
   // void Update()
   // {
        
   // }

    void OnCollisionEnter (Collision collision)
    {
        //collision.gameObject.tag == "CollidableObject"
        if (!collisionAudio.isPlaying && collision.relativeVelocity.magnitude>=2  )
        {
            collisionAudio.Play();
        }
    }
}