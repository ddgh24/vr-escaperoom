using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;
using UnityEngine.SceneManagement;

public class FunnelContains : MonoBehaviour
{

    [SerializeField] private Transform collideCheckTransform = null;
    [SerializeField] private LayerMask funnelMask;
    [SerializeField] private GameObject ing1;
    [SerializeField] private GameObject ing2;
    [SerializeField] private GameObject ing3;
    [SerializeField] private GameObject ing4;
    [SerializeField] private GameObject beaker;
    private int ingNum;
    private Collider ing1Col;
    private Collider ing2Col;
    private Collider ing3Col;
    private Collider ing4Col;
    private int ing1Check;
    private int ing2Check;
    private int ing3Check;
    private int ing4Check;
    private Transform acidTran;
    private GameObject acidObj;
    private Material acid;

    private PickUpItem pickupScript;

    public AudioSource funkyfunnel;
    public AudioClip tada;
    public AudioClip kaboom;

    private bool played=false;

    // Start is called before the first frame update
    void Start()
    {
        ing1Col = ing1.GetComponent<MeshCollider>();
        ing2Col = ing2.GetComponent<MeshCollider>();
        ing3Col = ing3.GetComponent<MeshCollider>();
        ing4Col = ing4.GetComponent<MeshCollider>();
        ing1Check = 0;
        ing2Check = 0;
        ing3Check = 0;
        ing4Check = 0;
        acidTran = beaker.transform.GetChild(1);
        acidObj = acidTran.gameObject;
        acid = acidObj.GetComponent<Renderer>().material;
        acid.SetFloat("Vector1_afbed82321df45abb1eccdfcb1bda448", 0.36f);

        acidObj = GameObject.Find ("myPotionPrefab");
        pickupScript = acidObj.GetComponent<PickUpItem>();
        pickupScript.enabled = false;
    }


    private void FixedUpdate() 
    {
        if (Physics.OverlapSphere(collideCheckTransform.position, 0.9f, funnelMask).Length == 4) {
            Collider[] hitColliders = Physics.OverlapSphere(collideCheckTransform.position, 0.9f, funnelMask);
            foreach (var hitCollider in hitColliders)
            {
                if (hitCollider == ing1Col && ing1Check == 0) {
                    ingNum++;
                    ing1Check = 1;
                }
                if (hitCollider == ing2Col && ing2Check == 0) {
                    ingNum++;
                    ing2Check = 1;
                }
                if (hitCollider == ing3Col && ing3Check == 0) {
                    ingNum++;
                    ing3Check = 1;
                }
                if (hitCollider == ing4Col && ing4Check == 0) {
                    ingNum++;
                    ing4Check = 1;
                }

            }
            if (ingNum < 4 && !played) {
                    funkyfunnel.PlayOneShot(kaboom);
                    played = true;
                    SceneManager.LoadScene("GameOver");
                
            }
            if (ingNum == 4) {
                StartCoroutine(waiter());
            }
        }

    
    }

    IEnumerator waiter() {
        yield return new WaitForSeconds(0.4f);
        ing1.SetActive (false);
        if (!played)
        {
            funkyfunnel.PlayOneShot(tada);
            played = true;
        }
        acid.SetFloat("Vector1_afbed82321df45abb1eccdfcb1bda448", 0.4f);
        yield return new WaitForSeconds(0.3f);
        ing2.SetActive (false);
        acid.SetFloat("Vector1_afbed82321df45abb1eccdfcb1bda448", 0.45f);
        yield return new WaitForSeconds(0.3f);
        ing3.SetActive (false);
        acid.SetFloat("Vector1_afbed82321df45abb1eccdfcb1bda448", 0.5f);
        yield return new WaitForSeconds(0.3f);
        ing4.SetActive (false);

        
        acid.SetFloat("Vector1_afbed82321df45abb1eccdfcb1bda448", 0.6f);

        pickupScript.enabled = true;
    }

}