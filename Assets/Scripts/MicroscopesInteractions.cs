using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicroscopesInteractions : ClickableObject
{
    public GameObject microscope;
    public GameObject popup;
    public GameObject slide;
    public bool slideInserted=false;
    public bool check=false;

    public AudioSource funkyslide;
    public AudioClip place;
    public AudioClip slideopen;
    float inputAcceleration;

    /* // Start is called before the first frame update
     new void Start()
     {
         slideInserted = false;
         check = false;
     }*/

    void Update()
    {
        if(popup.activeSelf == true)
        {
            if (!check)
            {
                wait();
                check = true;
            }
            else
            {
                if (VRInput.InteractLeft() || VRInput.InteractRight())
                {
                    check = false; 
                    popup.SetActive(false);
                    GameObject.Find("EscRoom-Player Variant").GetComponent<OVRPlayerController>().Acceleration = inputAcceleration;

                }
            }
        }
    }

    protected override void OnClickWithObject (string itemName)
    {
        if (slideInserted == false)
        {
            switch (itemName)
            {
                case "slide":
                    slideInserted = true;
                    inputAcceleration = GameObject.Find("EscRoom-Player Variant").GetComponent<OVRPlayerController>().Acceleration;
                    slide.SetActive(false);
                    funkyslide.PlayOneShot(place);
                    Destroy(PlayerPickUp.Instance.HeldItem);
                    break;

                case null:
                    break;

                default:
                    Debug.LogError("There is an object in objectsWithInteractions that has no interaction defined in OnClickWithItem");
                    break;
            }
        }
        else
        {
            OnClick();
        }
    }

    protected override void OnClick()
    {
        if (slideInserted == true && !check)
        {
            funkyslide.PlayOneShot(slideopen);
            GameObject.Find("EscRoom-Player Variant").GetComponent<OVRPlayerController>().Acceleration = 0;
            popup.SetActive(true);
            
        }
            

     }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(1f);
    }

}


