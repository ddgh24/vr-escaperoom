using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    public void StartGame() {
        SceneManager.LoadScene("lab");
    }

    public void ExitGame() {
        Application.Quit();
    }

    public void ReturnMain() {
        SceneManager.LoadScene("Play Menu");
    }
}
