using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioInteraction : ClickableObject
{
    public AudioSource radioAudio;

    public GameObject battery1;
    public GameObject battery2;


    bool battery1Inserted= false;
    bool battery2Inserted=false;
    bool isPlaying=false;

    public AudioClip correctClue;
    public AudioClip incorrectStatic;

    public bool correctPin = false; //boolean accessed by PinPad script, changed to true if pin is correct
    /*
    // Start is called before the first frame update
    public override void Start()
    {

        radioAudio = GetComponent<AudioSource>();
        radioAudio.loop = false;
        battery1Inserted = false;
        battery2Inserted = false;
        isPlaying = false;
    }*/

    protected override void OnClickWithObject (string itemName)
    {
        if (battery1Inserted == false || battery2Inserted == false)
        {
            switch (itemName)
            {
                case "battery1":
                    battery1Inserted = true;
                    battery1.SetActive(false);
                    Destroy(PlayerPickUp.Instance.HeldItem);
                    break;
                     
                case "battery2":
                    battery2Inserted = true;
                    battery2.SetActive(false);
                    Destroy(PlayerPickUp.Instance.HeldItem);
                    break;

                case null:
                    break;

                default:
                    Debug.LogError("There is an object in objectsWithInteractions that has no interaction defined in OnClickWithItem");
                    break;
            }
        }
        else
        {
            OnClick();
        }
    }

    protected override void OnClick()
    {
        if (battery1Inserted == true && battery2Inserted == true)
        {
            if (isPlaying == false && correctPin==false)//can be turned on, but not correct AKA STATIC CASE
            {
                radioAudio.loop = true;
                radioAudio.clip = incorrectStatic;
                radioAudio.Play();
                isPlaying = true;
                Debug.Log("Currently playing static");
            }
            else if (isPlaying == false && correctPin)//can be turned on, and pin correct AKA CORRECT CLUE CASE
            {
                radioAudio.loop = true;
                radioAudio.clip = correctClue;
                radioAudio.Play();
                isPlaying = true;
                Debug.Log("Currently playing clue");

            }
            else//is turned on, will be turned off AKA TURN OFF CASE
            {
                radioAudio.loop = false;
                radioAudio.Stop();
                isPlaying = false;
            }
        }
            

     }

 }


