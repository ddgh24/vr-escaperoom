using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private Transform groundCheckTransform = null;
    [SerializeField] private LayerMask playerMask;
    private bool jumpKeyWasPressed;
    private float horizontalInput;
    private float forwardBackwardInput;
    private Rigidbody rigidBodyComponent;


    // Start is called before the first frame update
    void Start()
    {
        rigidBodyComponent = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) {
            jumpKeyWasPressed = true;
        }

        horizontalInput = Input.GetAxis("Horizontal");
        forwardBackwardInput = Input.GetAxis("Vertical2");
    }

    private void FixedUpdate() {
       // rigidBodyComponent.velocity = new Vector3(horizontalInput, rigidBodyComponent.velocity.y, forwardBackwardInput);
        if (Physics.OverlapSphere(groundCheckTransform.position, 0.1f, playerMask).Length == 1) {
            print("return statement");
            return;
        }
        if (jumpKeyWasPressed) {
            rigidBodyComponent.velocity = new Vector3(horizontalInput, 10, forwardBackwardInput);
           // rigidBodyComponent.AddForce(Vector3.up*5, ForceMode.VelocityChange);
            jumpKeyWasPressed = false;
            print("jumpend");
        }
    }
}
