using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Hands {left = 0, right = 1};

public class HandController : MonoBehaviour
{
    [Tooltip("Is this the left hand or right?")]
    [SerializeField] Hands handtype = Hands.right;
    [Tooltip("How far should the renderer for raycast go forward?")]
    [SerializeField] float maxLength = 5; 
    [SerializeField] LineRenderer line;

    ClickableObject hoverObj = null;
    void Start() {
        line.SetPositions(new Vector3[2] {Vector3.zero, Vector3.zero});
    }
    void Update() {
        if (!GetInputRaycast()) {
            if (hoverObj) {
                hoverObj.OnEndHover();
                hoverObj = null;
            }
            line.enabled = false;
            return;
        }
        line.enabled = true;

        //click on object
        ClickableObject obj = GetRaycastObject();
        //handle turning on and off glow on objects
        ManageGlow(obj); 
        hoverObj = obj;
        //if input, interact
        if (obj && GetInputInteract()) {
            obj.Interact(PlayerPickUp.Instance.HeldItem, handtype); 
        }
    }

    private ClickableObject GetRaycastObject() {
        Vector3 endPosition = transform.position + transform.forward * maxLength;
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        ClickableObject clickObj = null;
        //raycast for object
        if (Physics.Raycast(ray, out hit)) {
            GameObject go = hit.collider.gameObject;
            if (!go) { Debug.LogError("no gameobject hit by raycast"); }
            clickObj = go.GetComponent<ClickableObject>();
            if (clickObj) { endPosition = hit.point; }
        } 
        //set positions
        line.SetPosition(0, transform.position);
        line.SetPosition(1, endPosition); 
        return clickObj;
    }
    private void ManageGlow(ClickableObject obj) {
            if (obj == null && hoverObj == null) { return; }
            bool bothExist = obj != null && hoverObj != null;
            if (bothExist && obj.gameObject == hoverObj.gameObject) {
                //we were already hovering on this before
                return;
            } 
            if (obj) {obj.OnHover(); }
            if (hoverObj) { hoverObj.OnEndHover(); }
        }
    private bool GetInputInteract() {
        if (handtype == Hands.right && VRInput.InteractRight()) return true;
        if (handtype == Hands.left && VRInput.InteractLeft()) return true;
        return false;    
    }
    private bool GetInputRaycast() {
        if (handtype == Hands.right && VRInput.GetRaycastRight()) return true;
        if (handtype == Hands.left && VRInput.GetRaycastLeft()) return true;
        return false;    
    }

}
